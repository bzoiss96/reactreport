import React from 'react';
import { useState, useEffect, useCallback } from 'react';

import ReactDatePicker from 'react-datepicker';

import ReportBase from './ReportBase';

const TestReport = () => {
    const [startDate, setStartDate] = useState(new Date());
    const [endDate, setEndDate] = useState(new Date());
    const [tableData, setTableData] = useState([]);

    const handleClick = (e) => {
        e.preventDefault();
        submitForm();
    };

    const submitForm = useCallback(async () => {
        // const response = await fetch('/'); fetch data with state
        const response = [
            { asset_name: '123', driver_name: 'Joe Smith', location: 'West 86th Street' },
            { asset_name: 'Dump Truck', driver_name: 'John Doe', location: 'Indianapolis' },
            { asset_name: 'Pickup Truck', driver_name: 'Don Capelli', location: 'New York City' },
        ]; // placeholder so it will render
        if (JSON.stringify(response) !== JSON.stringify(tableData)) setTableData(response);
    }, [tableData]);

    useEffect(() => {
        submitForm();
    }, [submitForm]);

    return (
        <ReportBase reportName='test_report' reportTitle='Test Report' tableData={tableData}>
            <div className='mb-3'>
                <label>Start Date</label>
                <ReactDatePicker selected={startDate} onChange={(date) => setStartDate(date)} />
            </div>
            <div className='mb-3'>
                <label>End Date</label>
                <ReactDatePicker selected={endDate} onChange={(date) => setEndDate(date)} />
            </div>
            <div className='mb-3'>
                <button className='btn btn-primary' onClick={handleClick}>
                    Submit
                </button>
            </div>
        </ReportBase>
    );
};

export default TestReport;
