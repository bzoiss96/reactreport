import React from 'react';

import TestReport from './report/TestReport';

const App = () => {
    return (
        <div>
            <TestReport />
        </div>
    );
};

export default App;
