Steps to run:

-   npm install
-   npm start

Flow to create a new basic report:

-   Create a new js file [ReportName.js]
-   Import ReportBase and add the form as a child of it
-   Create the state needed for the form
-   Setup the auto-load with useEffect
-   Create a function to grab the table data
-   Create a function to get the table data without refreshing the page
-   ReportBase only needs the title, name, and data in the table
