import React from 'react';

import ReportTable from './ReportTable';

const ReportBase = ({ reportTitle, reportName, children, tableData }) => {
    return (
        <div className='container-fluid'>
            <form>
                <div className='mb-3'>
                    <h2>{reportTitle}</h2>
                </div>
                {children}
            </form>
            <hr />
            <ReportTable reportName={reportName} tableData={tableData} />
        </div>
    );
};

export default ReportBase;
