import React from 'react';
import { useState, useEffect, useCallback } from 'react';

const ReportTable = ({ reportName, tableData }) => {
    const [columns, setColumns] = useState([]);

    const getColumns = useCallback(async () => {
        // const response = await fetch('/'); get the columns with reportName
        const response = [
            { asset_name: 'Equipment Number', driver_name: 'Driver', location: 'Location' },
        ]; // placeholder so it will render
        if (JSON.stringify(response) !== JSON.stringify(columns)) setColumns(response);
    });

    useEffect(() => {
        getColumns();
    }, [getColumns]);

    const renderHeader = () => {
        if (columns.length !== 1) return null;
        return Object.values(columns[0]).map((item, i) => <th key={i}>{item}</th>);
    };

    const renderData = (row) => {
        return Object.keys(columns[0]).map((item, i) => <td key={i}>{row[item]}</td>);
    };

    const renderBody = () => {
        if (tableData.length < 1) return null;
        return tableData.map((item, i) => <tr key={i}>{renderData(item)}</tr>);
    };

    return (
        <div className='container-fluid'>
            <table className='table table-striped table-hover table-bordered border-dark'>
                <thead>
                    <tr>{renderHeader()}</tr>
                </thead>
                <tbody>{renderBody()}</tbody>
            </table>
        </div>
    );
};

export default ReportTable;
